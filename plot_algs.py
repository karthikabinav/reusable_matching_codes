from numpy import *
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size': 16})
#matplotlib.rc('xtick', labelsize=20) 
#matplotlib.rcParams.update({'font.size': 20})

t = xrange(19)
#OPT = [471]*19
OPT = [600]*19
y_greedy = [204, 198, 175, 295, 230, 144, 124, 212, 248, 191, 221.5, 182, 161, 181, 263, 195, 211, 232, 206]    
y_uniform = [133, 123, 106, 208, 139, 101, 92, 136, 158, 116, 139, 110, 108, 111.5, 194, 132, 177, 161, 162]
y_LPheuristic = [210, 201, 177, 301, 232, 144, 123, 198, 250, 187, 229, 182, 160, 176, 263, 195, 210, 238, 204] 
y_LP = [215, 210, 180, 311, 244, 160, 150, 250, 300, 220, 250, 203.5, 201, 196, 300, 221, 243, 262.5, 236]
y_epsilonGreedy = [203, 203, 170, 302, 235, 151, 141, 230, 286, 198, 220, 191, 186, 182, 211, 217, 239, 253, 219]

#y_greedy = [204, 198, 175, 295, 230, 144, 124, 212, 248, 191, 221.5, 182, 161, 181, 263, 195, 211, 232, 206]    
#y_uniform = [133, 123, 106, 208, 139, 101, 92, 136, 158, 116, 139, 110, 108, 111.5, 194, 132, 177, 161, 162]
#y_LPheuristic = [235, 231, 206, 318, 260, 143, 152, 243, 284, 233, 262, 200, 208, 201, 289, 234, 251, 269, 245]
#y_LP = [330, 300, 264, 364, 297, 223, 193, 223, 265, 250, 288, 236, 241, 233, 299, 241, 263, 277, 249] 


#y_greedy = [204, 198, 175, 295, 230, 144, 124, 212, 248, 191, 221.5, 182, 161, 181, 263, 195, 211, 232, 206]    
#y_uniform = [133, 123, 106, 208, 139, 101, 92, 136, 158, 116, 139, 110, 108, 111.5, 194, 132, 177, 161, 162]
#y_LPheuristic = [241, 232, 205, 297, 271, 147, 162, 233, 277, 241, 252, 195, 211, 203, 293, 237, 263, 273, 241]
#y_LP = [331, 305, 261, 359, 301, 232, 207, 217, 271, 247, 274, 241, 238, 239, 302, 238, 269, 291, 237] 

#plt.plot(t,y_LP_2,label="ALG-LP normal dist") # plotting t,c separately 
#plt.plot(t,y_LP,label="ALG-LP powerlaw dist") # plotting t,c separately 

plt.plot(t, OPT, label="OPT")
plt.plot(t,y_greedy,label = "GREEDY") # plotting t,a separately 
plt.plot(t,y_uniform,label="UR-ALG") # plotting t,b separately 
plt.plot(t,y_LPheuristic,label="ALG-SC-LP") # plotting t,c separately 
plt.plot(t,y_LP,label="ALG-LP") # plotting t,c separately 
plt.plot(t,y_epsilonGreedy,label="e-GREEDY") # plotting t,c separately 
plt.ylabel('Average weight of the matching')
plt.xlabel('Test Data set number')
plt.legend(loc="best")
plt.ylim((0,700))
plt.savefig('normal_uniform.png')
