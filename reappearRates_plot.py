import math
from datetime import datetime, date
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
gr = 15

vert = "79F8BC483D5BD960CCF3169E6A42793C"

times = dict()

count = 0
for i in range(1, 32):
    f = open("trip_data_1/trip_data_1_" + str(i) + ".csv", "r")

    for line in f:
        vals = line.split(",")
        lhs = vals[1]
        if lhs != vert:
            continue
        times[count] = int(vals[8])/300.0
        count+=1
x = list()
y = list()
for key, value in times.iteritems():
    x.append(key)
    y.append(value)

t = np.array(x)
s = np.array(y)
plt.plot(t, s)

plt.xlabel('Trip number')
plt.ylabel('Number of Time-steps car unavailable')
plt.savefig(vert + '_figure.png')
print "Done"
