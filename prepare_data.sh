#!/bin/bash

fileName='normal_and_pjt'
granularity=15
T=615

python getLHSvertices.py $fileName
echo "LHS done"

python getRHSvertices.py $granularity $fileName
echo "RHS done"

#python getStatistics_powerlaw.py $granularity $fileName
python getStatistics.py $granularity $fileName
echo "statistics done"

python splitRHSvertices.py $fileName
echo "split RHS vertices"

python createEdgeWeight.py $fileName
echo "edgeweights done"

python obtain_pjt.py $T $granularity $fileName
echo "obtaining pjt"

python obtainLP_fast.py $T $fileName
echo "obtain LP done"

#matlab -nojvm < lp_solver.m
#echo "LP solver done"

