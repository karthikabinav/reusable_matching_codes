import math
from datetime import datetime, date
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
gr = 15
vert = "C617C18C8270F35E970788CFA4B976B0"

times = dict()


count = 0
for i in range(1, 32):
    f = open("trip_data_1/trip_data_1_" + str(i) + ".csv", "r")

    for line in f:
        vals = line.split(",")
        lhs = vals[1]
        if lhs!=vert:
            continue
        aa = int(vals[8])/300
        if aa not in times:
            times[aa] = 1
        else:
            times[aa] += 1 
x = list()
y = list()
for key, value in times.iteritems():
    x.append(key)
    y.append(value)

t = np.array(x)
s = np.array(y)
plt.plot(t, s, 'b')

vert = "79F8BC483D5BD960CCF3169E6A42793C"

times = dict()


count = 0
for i in range(1, 32):
    f = open("trip_data_1/trip_data_1_" + str(i) + ".csv", "r")

    for line in f:
        vals = line.split(",")
        lhs = vals[1]
        if lhs!=vert:
            continue
        aa = int(vals[8])/300
        if aa not in times:
            times[aa] = 1
        else:
            times[aa] += 1 
x = list()
y = list()
for key, value in times.iteritems():
    x.append(key)
    y.append(value)

t = np.array(x)
s = np.array(y)
plt.plot(t, s, 'r')

plt.xlabel('Number of Time-steps car unavailable')
plt.ylabel('Number of requests')


plt.savefig('two_specific_reappeardistribution_figure.png')
#plt.savefig('reappeardistribution_figure.png')
print "Done"
